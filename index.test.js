const app = require('./index.js');
const supertest = require('supertest');
const request = supertest(app);

it('Test the root path', async done => {
    const response = await request.get('/');
    expect(response.status).toBe(200);
    done();
});

it('Test the /page path', async done => {
  const response = await request.get('/page');
  expect(response.status).toBe(200);
  done();
});

it('Test the /world path', async done => {
  const response = await request.get('/world');
  expect(response.status).toBe(200);
  done();
});
