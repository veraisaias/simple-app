const express = require('express');
const app = express();

app.get('/', async (req, res) => {
  const today = new Date();
  res.send(`<h2 style="text-align:center">Today's date: ${today}</h2>`);
});

app.get('/page', (req, res) => {
  res.send(`<h2 style="text-align:center">You are in ${req.get('Host') + req.url}</h2>`);
});

app.get('/world', (req, res) => {
  res.send("<h2 style=\"text-align:center\">Hello World</h2>");
});

module.exports = app;
