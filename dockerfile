# partimos de una imagen
FROM node:10
# definimos el directorio de trabajo
WORKDIR /usr/src/app
# copiamos los ficheros con las dependencias a instalar
COPY package*.json ./
# instalamos las dependencias
RUN npm install --only=production
# copiamos todo nuestro proyecto
COPY . .
# iniciamos la aplicación
ENTRYPOINT npm start
